import express from 'express';
import mongoose from "mongoose";
import {Config} from "./config";

export function handleError(reason: string, res: express.Response, code: number = 400) {
    res.status(code);
    res.json({
        "status": "error",
        "reason": reason
    });
}

export function connectToDB(): void {
    mongoose.connect(Config.instance.mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    }, (err: any) => {
        if (err) {
            console.error("Can not connect to database!");
            console.error(err.message);
            process.exit();
        } else {
            console.log("Successfully connected to database!");
        }
    });
}

export function disconnectDB(): void {
    mongoose.disconnect();
    console.log("Successfully disconnected from database!");
}