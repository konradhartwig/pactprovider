import * as mongoose from "mongoose";

export interface ICustomer extends mongoose.Document {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
}

const CustomerSchema = new mongoose.Schema({
    id: { type: String, required: true, index: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
}, {
    versionKey: false
});

CustomerSchema.set("toJSON", {
    virtuals: true,
    transform (doc, ret) { delete ret._id }
});

const Customer = mongoose.model<ICustomer>("Customer", CustomerSchema);
export default Customer;