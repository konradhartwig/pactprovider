import Handler from "../handlers/handler";
import express from "express";
import Customer from "../types/customer";
import * as utils from "../utils";

export default class StateHandler extends Handler {
    constructor() {
        super();
        this.getRouter().post("/dev/state", this.setState.bind(this));
    }

    private async setState(req: express.Request, res: express.Response): Promise<void> {
        try {
            const consumer = req.body.consumer;
            const state = req.body.state;
            switch (consumer) {
                case "DemoConsumer":
                    switch (state) {
                        case "I have a list of customers":
                            // clear data
                            Customer.remove({});

                            // insert data
                            await Customer.create({
                                // @ts-ignore
                                "id": "1",
                                "firstName": "Max",
                                "lastName": "Mustermann",
                                "email": "noreply@mustermannmax.com"
                            });

                            res.send("Customers added to db");
                            break;
                        case "Init state":
                            res.send("Server in init state");
                            break;
                    }
                    break;
                default:
                    throw new Error("Could not find state");
            }
        } catch (e) {
            utils.handleError(e.message, res);
        }
    }
}