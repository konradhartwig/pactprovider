import Handler from "./handler";
import express from 'express';
import Customer from "../types/customer";
import * as utils from "../utils";

export default class CustomerHandler extends Handler {
    constructor() {
        super();
        this.getRouter().get("/customers", this.getAll.bind(this));
        this.getRouter().post("/customer", this.create.bind(this));
    }

    private async getAll(req: express.Request, res: express.Response): Promise<void> {
        try  {
            const customer = await Customer.find();
            res.json(customer);
        } catch (e) {
            utils.handleError(e.message, res);
        }
    }

    private async create(req: express.Request, res: express.Response): Promise<void> {
        try {
            const customer = await Customer.create(req.body);
            res.json({
                "status": "success",
                "customerId": customer.id
            })
        } catch (e) {
            utils.handleError(e.message, res);
        }
    }

}