import axios, {AxiosPromise} from "axios";
import {Config} from "../config";

export class CustomerClient {
    private url: string;
    private port: string;

    constructor(endpoint: any) {
        this.url = endpoint.url;
        this.port = endpoint.port;
    }
    public getCustomers = () : AxiosPromise => {
        return axios.request({
            baseURL: `${this.url}:${this.port}`,
            headers: { Accept: "application/json" },
            method: "GET",
            url: "/customers",
        })
    }
}